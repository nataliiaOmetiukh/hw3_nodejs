const express = require('express');
const router = new express.Router();

const {
  login,
  registration,
  changePassword,
} = require('./../controllers/authController');
const {asyncWrapper} = require('./helpers/errorHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateRegistration} = require('./middlewares/validateRegistration');

router.post('/auth/register', validateRegistration, asyncWrapper(registration));
router.post('/auth/login', asyncWrapper(login), authMiddleware);
router.post('/auth/forgot_password', asyncWrapper(changePassword));


module.exports = router;

