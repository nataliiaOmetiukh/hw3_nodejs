const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    required: true,
    type: String,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    required: true,
    default: 'IS',
    enum: ['IS', 'OL'],
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
