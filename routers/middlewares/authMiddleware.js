const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

module.exports.authMiddleware = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(401)
        .json({massage: `No Authorization http header found`});
  }

  const token = req.headers.authorization.split(' ')[1];

  if (!token) {
    return res.status(401).json({massage: `No JWT token found`});
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};
