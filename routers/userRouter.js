const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();
const {authMiddleware} = require('./middlewares/authMiddleware');
const {User} = require('../models/userModel');

router.get('/', authMiddleware, async (req, res) => {
  const user = await User.findById({_id: req.user._id})
      .select({'email': 1, 'created_date': 1});

  if (!user) {
    return res.status(400).json({message: 'Please autheticate'});
  }

  res.status(200).json({user});
});

router.delete('/', authMiddleware, async (req, res) => {
  try {
    await User.deleteOne({_id: req.user._id});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  res.status(200).json({message: 'User removed successfully'});
});

router.patch('/password', authMiddleware, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Wrong password'});
  }
  if (newPassword) {
    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
    return res.status(200).json({message: 'Password changed successfully'});
  }
});

module.exports = router;
