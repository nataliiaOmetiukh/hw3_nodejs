const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    required: true,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {
      width: {
        type: Number,
        required: true,
      },
      length: {
        type: Number,
        required: true,
      },
      height: {
        type: Number,
        required: true,
      },
    },
    required: true,
  },
  logs: {
    type: [
      {
        message: {
          type: String,
          default: 'Load successfully created',
        },
        time: {
          type: Date,
          default: new Date(),
        },
      },
    ],
    default: [],
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
