const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email({minDomainSegments: 2}),

    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{5,30}$')),

    role: Joi.string().valid('DRIVER', 'SHIPPER'),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};
