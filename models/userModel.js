const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ['DRIVER', 'SHIPPER'],
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
