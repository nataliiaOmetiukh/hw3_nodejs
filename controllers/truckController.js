const {Truck} = require('./../models/truckModel');

module.exports.getAllTrucks = async (req, res) => {
  let trucks;

  try {
    trucks = await Truck.find({created_by: req.user._id})
        .select({'__v': 0});
  } catch (err) {
    return res.status(400).json({message: 'Error'});
  }

  res.status(200).json({trucks});
};

module.exports.addTruck = async (req, res) => {
  const {type} = req.body;

  const truck = new Truck({
    created_by: req.user._id,
    type,
  });

  truck.save();

  res.status(200).json({message: 'Successfully'});
};

module.exports.getTruckById = async (req, res) => {
  const id = req.params.id;

  const truck = await Truck.findById(id, {_v: 0}).exec();

  if (!truck) {
    return res.status(400).json({message: 'Error'});
  }

  res.status(200).json({truck});
};

module.exports.updateTruckById = async (req, res) => {
  const id = req.params.id;
  const type = req.body.type;

  const truck = await Truck.findById(id, {_v: 0}).exec();

  if (!truck || truck.status === 'OL' ) {
    return res.status(400).json({message: 'Client error'});
  }

  await truck.updateOne({type});

  truck.save();

  res.status(200).json({message: 'Changed successfully'});
};

module.exports.deleteTruckById = async (req, res) => {
  const id = req.params.id;

  await Truck.findByIdAndRemove(id);
  res.status(200).json({message: 'Truck deleted successfully!'});
};

module.exports.assignTruckById = async (req, res) => {
  const id = req.params.id;
  const {_id: userId} = req.user;

  const previousAssigned = await Truck.findOne({
    created_by: userId,
    assigned_to: userId,
  }) || null;

  if (previousAssigned) {
    await previousAssigned.updateOne({assigned_to: null});
  }

  const currentAssigned = await Truck.findOne({_id: id, created_by: userId});

  if (!currentAssigned) {
    return res.status(400).json({message: 'No found truck with this id'});
  }

  await currentAssigned.updateOne({assigned_to: userId});

  currentAssigned.save();

  return res.status(200).json({message: 'Assigned successfully'});
};
