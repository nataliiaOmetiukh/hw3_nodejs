const Joi = require('joi');

module.exports.validateTruckType = async (req, res, next) => {
  const {type} = req.body;

  const schema = Joi.object({
    type: Joi.string().valid('SPRINTER', 'LARGE STRAIGHT', 'SMALL STRAIGHT'),
  });

  try {
    await schema.validateAsync({type});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};
