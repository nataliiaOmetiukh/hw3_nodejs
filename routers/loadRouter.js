const express = require('express');
const {
  addLoad,
  getAllLoads,
  getActiveLoad,
  changeLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
} = require('../controllers/loadController');
const {asyncWrapper} = require('./helpers/errorHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  checkShipperMiddleware,
  checkDriverMiddleware,
} = require('./middlewares/checkRoleMiddleware');
const router = new express.Router();

router.get('/loads', authMiddleware, asyncWrapper(getAllLoads));
router.post(
    '/loads',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(addLoad),
);
router.get(
    '/loads/active',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(getActiveLoad),
);
router.patch(
    '/loads/active/state',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(changeLoadState),
);
router.get('/loads/:id', authMiddleware, asyncWrapper(getLoadById));
router.put(
    '/loads/:id',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(updateLoadById),
);
router.delete(
    '/loads/:id',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(deleteLoadById),
);
router.post(
    '/loads/:id/post',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(postLoadById),
);
router.get(
    '/loads/:id/shipping_info',
    authMiddleware,
    checkShipperMiddleware,
    asyncWrapper(getLoadShippingInfoById),
);

module.exports = router;
