/* eslint-disable camelcase */
const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

const {
  updateState,
  changeLoadStatus,
} = require('../routers/helpers/loadHelper');
const {
  getSizeByType,
  compareSizes,
} = require('../routers/helpers/sizeHelper');


module.exports.addLoad = async (req, res) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const userCredentials = req.user;

  const availableTruck = await Truck.findOne({status: 'IS'});

  if (!availableTruck) {
    return res
        .status(400)
        .json({message: 'There are no available trucks at this moment'});
  }

  const truckSize = getSizeByType(availableTruck.type);
  const loadFits = compareSizes({...truckSize}, {...dimensions, payload});

  if (!loadFits) {
    return res
        .status(400)
        .json({message: 'There are no available trucks at this moment'});
  }

  const load = new Load({
    created_by: userCredentials._id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {...dimensions},
  });

  load.save();

  res.status(200).json({message: 'Load created successfully'});
};

module.exports.getAllLoads = async (req, res) => {
  const {_id: id} = req.user;
  const {status = null, limit = 10, offset = 0} = req.query;

  if (+limit > 50) {
    limit = 50;
  }

  const user = await User.findOne({_id: id});

  let loads = [];

  if (user.role === 'SHIPPER') {
    loads = await Load.find({created_by: id})
        .skip(+offset)
        .limit(+limit)
        .select({'__v': 0, 'logs._id': 0});
  }

  if (user.role === 'DRIVER') {
    const driverTrucks = await Truck.find({created_by: id});

    for (const truck of driverTrucks) {
      const loadsByTruck =
                (await Load.find({assigned_to: truck._id}).select({
                  '__v': 0,
                  'logs._id': 0,
                })) || [];

      if (loadsByTruck) {
        loads = loads.concat(loadsByTruck);
      }
    }

    const numOfLoads = +offset + +limit;
    loads = loads.slice(+offset, numOfLoads);
  }

  if (status) {
    loads = loads.filter((load) => load.status === status);
  }

  res.status(200).json({loads});
};

module.exports.getActiveLoad = async (req, res) => {
  const {_id: id} = req.user;

  const assignedTruck = await Truck.findOne({
    created_by: id,
    assigned_to: id,
  });

  if (!assignedTruck) {
    return res.status(400).json({message: 'No assigned truck found'});
  }

  const load = await Load.findOne({
    assigned_to: assignedTruck._id,
    status: 'ASSIGNED',
  }).select({'__v': 0, 'logs._id': 0});

  if (!load) {
    return res.status(400).json({message: 'No active loads'});
  }

  res.status(200).json({load});
};

module.exports.changeLoadState = async (req, res) => {
  const {_id: id} = req.user;

  const assignedTruck = await Truck.findOne({assigned_to: id});

  if (!assignedTruck) {
    return res.status(400).json({message: 'No assigned trucks found'});
  }

  const load = await Load.findOne({
    assigned_to: assignedTruck._id,
    status: 'ASSIGNED',
  });

  if (!load) {
    return res.status(400).json({message: 'No assigned loads found'});
  }

  let newState;

  switch (load.state) {
    case 'En route to Pick Up':
      newState = await updateState(load, 'Arrived to Pick Up');
      break;
    case 'Arrived to Pick Up':
      newState = await updateState(load, 'En route to delivery');
      break;
    case 'En route to delivery':
      newState = await updateState(load, 'Arrived to delivery');
      break;
    default:
      return res.status(400).json({
        message: `Load is already delivered`,
      });
  }

  if (newState === 'Arrived to delivery') {
    await assignedTruck.updateOne({status: 'IS'});
    assignedTruck.save();
    await changeLoadStatus(load, 'SHIPPED', 'Load was delivered');
  }

  res.status(200).json({
    message: `Load state changed to '${newState}'`,
  });
};

module.exports.getLoadById = async (req, res) => {
  const {id} = req.params;

  const load = await Load.findOne({_id: id}).select({
    '__v': 0,
    'logs._id': 0,
  });

  if (!load) {
    return res.status(400).json({message: 'No load with such id'});
  }

  res.status(200).json({load});
};

module.exports.updateLoadById = async (req, res) => {
  const {id} = req.params;
  const fieldsToUpdate = req.body;

  const load = await Load.findOne({_id: id});

  if (!load) {
    return res.status(400).json({message: 'No load with such id'});
  }

  await load.updateOne({...fieldsToUpdate});

  load.save();

  res.status(200).json({message: 'Load details changed successfully'});
};

module.exports.deleteLoadById = async (req, res) => {
  const {id} = req.params;

  try {
    await Load.deleteOne({_id: id});
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  res.status(200).json({message: 'Load deleted successfully'});
};

module.exports.postLoadById = async (req, res) => {
  const {id} = req.params;

  const currentLoad = await Load.findOne({_id: id, status: 'NEW'});

  if (!currentLoad) {
    return res
        .status(400)
        .json({message: 'Load with this id is not found'});
  }

  await changeLoadStatus(
      currentLoad,
      'POSTED',
      'Load changed its status to POSTED',
  );

  const availableTruck = await Truck.findOne({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  if (!availableTruck) {
    await changeLoadStatus(
        currentLoad,
        'NEW',
        'Load changed its status to NEW',
    );

    return res
        .status(400)
        .json({message: 'There are no available trucks at this moment'});
  }

  const truckSize = getSizeByType(availableTruck.type);

  const loadFits = compareSizes(
      {...truckSize},
      {...currentLoad.dimensions, payload: currentLoad.payload},
  );

  if (!loadFits) {
    await changeLoadStatus(
        currentLoad,
        'NEW',
        'Load changed its status to NEW',
    );

    return res
        .status(400)
        .json({message: 'There are no available trucks at this moment'});
  }

  await availableTruck.updateOne({status: 'OL'});
  availableTruck.save();

  await currentLoad.updateOne({
    state: 'En route to Pick Up',
    assigned_to: availableTruck._id,
  });

  await changeLoadStatus(
      currentLoad,
      'ASSIGNED',
      `Load assigned to driver with id ${availableTruck.created_by}`,
  );

  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

module.exports.getLoadShippingInfoById = async (req, res) => {
  const {id} = req.params;

  const load = await Load.findOne({_id: id}).select({
    '__v': 0,
    'logs._id': 0,
  });

  if (!load) {
    return res.status(400).json({message: 'No active load with such id'});
  }

  const truck =
        (await Truck.findOne({_id: load.assigned_to}).select({__v: 0})) ||
        'Not assigned yet or was deleted';

  res.status(200).json({load, truck});
};

