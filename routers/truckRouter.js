const express = require('express');
const {
  getAllTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/truckController');
const {asyncWrapper} = require('./helpers/errorHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {checkDriverMiddleware} = require('./middlewares/checkRoleMiddleware');
const {validateTruckType} = require('./middlewares/validateTruckMiddleware');
const router = new express.Router();

router.get(
    '/trucks',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(getAllTrucks),
);
router.post(
    '/trucks',
    authMiddleware,
    checkDriverMiddleware,
    validateTruckType,
    asyncWrapper(addTruck),
);
router.get(
    '/trucks/:id',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(getTruckById),
);
router.put(
    '/trucks/:id',
    authMiddleware,
    checkDriverMiddleware,
    validateTruckType,
    asyncWrapper(updateTruckById),
);
router.delete(
    '/trucks/:id',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(deleteTruckById),
);
router.post(
    '/truck/:id/assign',
    authMiddleware,
    checkDriverMiddleware,
    asyncWrapper(assignTruckById),
);

module.exports = router;
