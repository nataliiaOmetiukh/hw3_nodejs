const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');


const port = process.env.PORT || 8080;

app.use(express.json());
app.use(morgan('tiny'));
app.use(express.static('build'));

app.use('/api', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);


app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(
      'mongodb+srv://Natalifm:hwapp@cluster0.ome58.mongodb.net/hw3-db?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );
  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
