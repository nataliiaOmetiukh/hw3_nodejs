const Joi = require('joi');

module.exports.validatePassword = async (req, res, next) => {
  const {newPassword} = req.body;

  const schema = Joi.object({
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  try {
    await schema.validateAsync({password: newPassword});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};

