module.exports.checkDriverMiddleware = (req, res, next) => {
  if (req.user.role !== 'DRIVER') {
    return res.status(400).json({message: 'Only for drivers'});
  }

  next();
};

module.exports.checkShipperMiddleware = (req, res, next) => {
  if (req.user.role !== 'SHIPPER') {
    return res.status(400).json({message: 'Only for shippers'});
  }

  next();
};
