const passwordGenerator = require('generate-password');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const jwt = require('jsonwebtoken');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  if (email && password) {
    let user = await User.findOne({email});
    if (user) {
      res.status(400).json({message: 'User is already exist'});
    } else {
      user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role,
      });

      await user.save();

      res.status(200).json({message: 'User created successfully!'});
    }
  } else {
    res.status(400).json({message: 'Please enter all credentials'});
  }
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res
        .status(400)
        .json({message: `No user with username '${email}' found!`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const JWT_TOKEN = jwt.sign(
      {email: user.email, _id: user._id, role: user.role},
      JWT_SECRET,
  );

  res.header('authorization', JWT_TOKEN);
  res.status(200).json({jwt_token: JWT_TOKEN});
};

module.exports.changePassword = async (req, res) => {
  const {email} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `No user with email '${email}' was found!`,
    });
  }

  const newPassword = passwordGenerator.generate({
    length: 10,
    numbers: true,
  });

  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});
  user.save();

  res.status(200).json({message: 'New password sent to your email address'});
};

